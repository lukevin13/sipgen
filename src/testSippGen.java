import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.Test;

public class testSippGen {

	@Test
	public void testProcess() {
		
		SippGen.scenarioName = "Test Process";
		
		List<SippEntity> entities = new ArrayList<SippEntity>();

		for (int i = 0; i < 5; i++) {
			SippEntity se = new SippEntity();
			if (i % 2 == 0) {
				se.command = "send";

				HashMap<String, String> map = new HashMap<String, String>();
				map.put("retrans", "500");
				map.put("start_rtd", "7");
				se.attributes = map;

				se.content = "Here is some content at i=" + i;
			} else {
				se.command = "recv";
				
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("test", "true");
				map.put("next", "99");
				
				se.content = "Here is a some response at i=" + i;
				
			}
			entities.add(se);
		}
		
		try {
			SippGen.process(entities);
		} catch (IOException e) {
			System.err.println("Caught IOException in process");
		}
		
		assert(true);
	}

}
