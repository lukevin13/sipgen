import java.util.HashMap;

public class SippEntity {
	
	public String command;
	public HashMap<String, String> attributes;
	public String content;
	
	public SippEntity() {
		command = "";
		attributes = new HashMap<String, String>();
		content = "";
	}
	
	// Getters
	public String getCommand() {
		return command;
	}

	public HashMap<String, String> getAttributes() {
		return attributes;
	}

	public String getContent() {
		return content;
	}
	
	public boolean hasContent() {
		return !content.isEmpty();
	}

}