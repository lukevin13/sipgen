import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class SippGen {
	
	// Scenario Name
	public static String scenarioName;

	public static void main(String args[]) {
		scenarioName = "";
		
		SippGenGui sgg = new SippGenGui();
		
	}

	public static void process(List<SippEntity> entities) throws IOException {
		
		// Initiate the start of the XML file
		String xmlText = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>" + "\n"
				+ "<!DOCTYPE scenario SYSTEM \"sipp.dtd\">" + "\n\n"
						+ "<scenario name=\"" + scenarioName + "\">" + "\n\n";

		for (SippEntity entity : entities) {
			
			// Get and write the command
			String command = entity.getCommand();
			String seg = "\t<" + command;
			
			// Get and write the attributes
			Map<String,String> attributes = entity.getAttributes();
			for (String key : attributes.keySet()) {
				if (!key.equals("action") && !key.equals("cdata")) {
					seg += " " + key + "=\"" + attributes.get(key) + "\"";
				}
			}
			seg += ">\n";
			
			// Get and write the body content
			if (entity.hasContent()) {
				seg += "\t\t" + entity.getContent();
			}
			
			// Close up the segment
			seg += "\n\t</" + command + ">";
			xmlText += seg + "\n\n";
		}
		
		// Close up the XML Document
		xmlText += "</scenario>";
		
		// Create and write to file
		File file = new File(scenarioName + ".xml");
		if (!file.exists() || !file.isFile()) file.createNewFile();
		FileWriter writer = new FileWriter(file);
		writer.write(xmlText);
		writer.close();
	}

}