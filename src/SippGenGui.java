import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.*;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class SippGenGui {
	
	public JFrame appWindow;
	
	public SippGenGui() {
		// Options
		String[] commands = {"send", "recv", "pause", "nop", 
				"sendCmd", "recvCmd", "label", "Response Time Partition", 
				"Call Length Repartition", "Globals", "User", "Reference"};
		
		String[] com_attr = {"start_rtd", "rtd", "repeat_rtd", "crlf", "next",
				"test", "chance", "condexec", "condexec_inverse", "counter"};
		
		
		
		String[] send_attr = {"retrans", "lost", "start_txn", "ack_txn"};
		String[] recv_attr = {"response", "request", "optional", "rrs", "auth", "lost",
				"timeout", "ontimeout", "action", "regexp_match", "response_txn"};
		String[] pause_attr = {"milliseconds", "variable", "distribution", "sanity_check"};
		String[] nop_attr = {"action"};
		String[] sendCmd_attr = {"cdata", "dest"};
		String[] recvCmd_attr = {"action", "src"};
		String[] label_attr = {"id"};
		String[] rtp_attr = {"value"};
		String[] clp = {"value"};
		String[] globals_attr = {"variables"};
		String[] user_attr = {"variables"};
		String[] ref_attr = {"variables"};
		
		
		// Initialize the application window
		appWindow = new JFrame("SIPp XML Generator");
		appWindow.setSize(1200, 600);
		appWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		appWindow.setResizable(false);
		
		// Command dropdown
		JComboBox commandList = new JComboBox(commands);
		commandList.setSelectedIndex(0);
		
		JPanel panel = new JPanel();
		panel.add(commandList);
		
		appWindow.add(panel);
		appWindow.setVisible(true);
		
	}
}